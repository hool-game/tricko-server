import redis from "redis";
import bluebird from "bluebird";
import dotenv from "dotenv";
import { TrickoTransmitter } from "./xmpp.js";
import { TrickoEngine } from "./engine.js";

// For checking that the process is running
import url from "url";

// Make Resis support async
bluebird.promisifyAll(redis);

// load config from .env file (if exists)
dotenv.config();

const XMPP_SERVICE = process.env.TRICKO_XMPP_SERVICE;
const DOMAIN = process.env.TRICKO_COMPONENT_DOMAIN;
const PASSWORD = process.env.TRICKO_COMPONENT_PASSWORD;
const TITLE = process.env.TRICKO_COMPONENT_TITLE || "TRICKO Rooms";

async function start() {
  console.log("Initiaelising the server");

  // set up xmpp client
  const transmitter = new TrickoTransmitter({
    service: XMPP_SERVICE,
    domain: DOMAIN,
    password: PASSWORD,
  });
  
  // set up database connection
  const db = redis.createClient();

  // set up tricko engine
  const tricko = new TrickoEngine({
    db: db,
  });


  // link everything together
  tricko.listen(transmitter);
  transmitter.listen(tricko);

  // start the database
  console.log("Setting up the daetabase");
  await db.connect();

  // start the component
  console.log(`Opening connaection as ${XMPP_SERVICE}`);
  await transmitter.start().catch((err) => {
    console.error(`Oh no! An aerror occurred: ${err.message}`);
    process.exit();
  });

  console.log("Let the gaemes begin!");

  // handle termination
  process.once("SIGINT", async (code) => {
    console.log("Time to aexit");

    await transmitter.stop();
    console.log("Component disconnaected");

    console.log("Faerwell, folks!");
  });
}

// run if main script
if (process.argv?.[1] == url.fileURLToPath(import.meta.url)) {
  start();
}

export {
  TrickoTransmitter,
  TrickoEngine,
}