import { component, xml, jid } from "@xmpp/component";
import debug from "@xmpp/debug";

import { EventEmitter } from "events";

/**
 * Tricko Transmitter
 *
 * Acts as a bridge to receive XMPP events and pass
 * them on to the main process, and vice versa.
 */
export class TrickoTransmitter extends EventEmitter {
  constructor(options, ...args) {
    // initial settings go here
    super(...args);

    if (!options) throw "No options defined";
    if (!options.domain) throw "Please specify a domain";
    if (!options.service) throw "Please specify an XMPP service";
    if (!options.password) throw "Please specify a password!";

    if (!options.standard)
      options.standard = "http://hool.org/protocol/mug/tricko";

    this.standard = options.standard;

    this.service = options.service;
    this.domain = options.domain;

    this.xmpp = component({
      service: this.service,
      domain: this.domain,
      password: options.password,
    });

    console.debug(`Connecting as ${this.domain} via ${this.service}`);
    // setup event catching

    this.xmpp.on("error", (err) => {
      console.error(err);
    });

    this.xmpp.on("offline", () => {
      console.log("offline");
    });

    // handle incoming messages

    this.xmpp.on("stanza", async (stanza) => {
      // ignore IQs, since they're handled separately
      if (stanza.is("iq")) return;

      // ignore own messages
      var from = stanza.attrs.from ? jid(stanza.attrs.from) : undefined;
      if (from && from == this.domain) {
        console.log(`ignoring own message: ${stanza}`);
        return;
      }

      console.debug(`got stanza: ${stanza}`);

      // incoming presence (implicit)
      if (stanza.is("presence")) {
        // don't respond to presences to main domain
        if (stanza.attrs.to == this.domain) return;

        // ignore error stanzas, since we can't do anything about them
        if (stanza.getChildren("error").length) {
          console.warn(`Dropping error presence: ${stanza}`);

          return;
        }

        // handle "unavailable" stanzas: roomDisconnect
        if (stanza.attrs.type == "unavailable") {
          // check if it's a permament leaving
          let permanent = false;
          if (
            stanza.getChildren("game").length &&
            stanza.getChild("game").getChildren("item").length &&
            stanza.getChild("game").getChild("item").attrs.affiliation == "none"
          ) {
            permanent = true;
          }

          if (permanent) {
            this.emit("roomExit", {
              room: jid(stanza.attrs.to).bare().toString(),
              user: jid(stanza.attrs.from).bare().toString(),
            });
          } else {
            this.emit("roomDisconnect", {
              room: jid(stanza.attrs.to).bare().toString(),
              user: jid(stanza.attrs.from).bare().toString(),
            });
          }
          return;
        }

        // check for additional details
        if (stanza.getChildren("game").length) {
          let game = stanza.getChild("game");

          // check that we want to play a tricko game
          if (game.attrs.var != this.standard) {
            console.warn(`Ignoring non-Tricko stanza: ${stanza}`);
            return; // not supported for this component!
          }

          // get values
          let options = game.getChildren("item");
          if (options.length) {
            console.debug(`Selected role: ${options[0].attrs.role}`);
          }

          // check if player
          let role;

          if (
            ["player", "watcher"].some((role) => options[0].attrs.role == role)
          ) {
            role = options[0].attrs.role;
          } else {
            role = null;
          }

          // compute room and nickname
          let room = jid(stanza.attrs.to).bare();
          let nickname = jid(stanza.attrs.to).resource;

          if (!nickname) {
            nickname = jid(stanza.attrs.from).local;
          }

          // Broadcast the event
          this.emit("roomJoin", {
            room: room,
            user: jid(stanza.attrs.from).bare().toString(),
            role: role,
            nickname: nickname,
          });
        }
      }

      // incoming message
      else if (stanza.is("message")) {
        // handle the "start" command
        if (stanza.getChildren("start").length) {
          let start = stanza.getChild("start");

          if (start.attrs.xmlns == "http://jabber.org/protocol/mug#user") {
            this.emit("start", {
              room: jid(stanza.attrs.to).bare().toString(),
              user: jid(stanza.attrs.from).bare().toString(),
            });
          }
        }

        // handle turns (moves made by user)
        else if (stanza.getChildren("turn").length) {
          let turn = stanza.getChild("turn");        
          // card
          let card = turn.getChild("card");
          if (card && card.attrs.xmlns == this.standard) {
            this.emit("card", {
              room: jid(stanza.attrs.to).bare().toString(),
              user: jid(stanza.attrs.from).bare().toString(),
              rank: card.attrs.rank,
              suit: card.attrs.suit,
            });
          }

          // flip
          let flip = turn.getChild("flip");
          if (flip && flip.attrs.xmlns == this.standard) {
            this.emit("flip", {
              room: jid(stanza.attrs.to).bare().toString(),
              user: jid(stanza.attrs.from).bare().toString(),
            });
          }

          // take
          let take = turn.getChild("take");
          if (take && take.attrs.xmlns == this.standard) {
            this.emit("take", {
              room: jid(stanza.attrs.to).bare().toString(),
              user: jid(stanza.attrs.from).bare().toString(),
            });
          }
          let flipAndTrump =turn.getChild("flipandtrump")
          if(flipAndTrump && flipAndTrump.attrs.xmlns == this.standard){
            
             let trumpEl =flipAndTrump.children[0];
             console.log(flipAndTrump,flipAndTrump.children,trumpEl,"new method debug")
            this.emit("flipAndTrump", {
              room: jid(stanza.attrs.to).bare().toString(),
              user: jid(stanza.attrs.from).bare().toString(),
              value: trumpEl,
            });
          } 
          let trump =turn.getChild("trump")
          if(trump && trump.attrs.xmlns == this.standard){
            
             let trumpEl =trump.children[0];
             console.log(trump,trump.children,trumpEl,"new method debug")
            this.emit("trump", {
              room: jid(stanza.attrs.to).bare().toString(),
              user: jid(stanza.attrs.from).bare().toString(),
              value: trumpEl,
            });
          } 

        }
      }

      // handle groupchats
      if (
        stanza.attrs.type == "groupchat" &&
        stanza.getChildren("body").length
      ) {
        this.emit("chat", {
          from: jid(stanza.attrs.from).bare().toString(),
          room: jid(stanza.attrs.to).bare().toString(),
          id: stanza.attrs.id,
          text: stanza.getChild("body").text(),
        });
      }
    });

    // handle incoming IQs

    // express support for the tricko game
    this.xmpp.iqCallee.get(
      "http://jabber.org/protocol/disco#info",
      "query",
      async (ctx) => {
        // check if it's the root list
        if (ctx.stanza.attrs.to == this.domain) {
          console.debug(`returning root list`);
          return (
            <query xmlns="http://jabber.org/protocol/disco#info">
              <identity
                category="game"
                name={this.componentTitle}
                type="multi-user"
              />
              <feature var="http://jabber.org/protocol/disco#items" />
              <feature var="jabber:iq:search" />
              <feature var="jabber:iq:register" />
              <feature var="http://jabber.org/protocol/mug" />
              <feature var={this.standard} />
            </query>
          );
        } else {
          let result = await this.engine.getRoom(ctx.stanza.attrs.to);

          if (!result) {
            // if no result, return blank response
            return <query xmlns="http://jabber.org/protocol/disco#items" />;
          } else {
            // process info
            let playerCount = result.occupants.filter(
              (o) => o.role == "player"
            ).length;
            let watcherCount = result.occupants.filter(
              (o) => o.role == "watcher"
            ).length;

            // return info
            return (
              <query xmlns="http://jabber.org/protocol/disco#items">
                <feature var="http://jabber.org/protocol/disco#info" />
                <feature var="http://jabber.org/protocol/mug" />
                <feature var={this.standard} />
                <x xmlns="jabber:x:data" type="result">
                  <field var="FORM_TYPE" type="hidden">
                    <value>http://jabber.org/protocol/mug#matchinfo</value>
                  </field>
                  <field var="mug#match_description" label="Description">
                    <value>{result.name}</value>
                  </field>
                  <field var="tricko#player_count" label="Players">
                    <value>{watcherCount}</value>
                  </field>
                  <field var="tricko#watcher_count" label="Watchers">
                    <value>{kibitzerCount}</value>
                  </field>
                </x>
              </query>
            );
          }
        }
      }
    );

    // list active public tables
    this.xmpp.iqCallee.get(
      "http://jabber.org/protocol/disco#items",
      "query",
      async (ctx) => {
        // check if it's the root list
        if (ctx.stanza.attrs.to == this.domain) {
          console.debug("listing public rooms");
          let results = await this.engine.getRooms();
          return (
            <query xmlns="http://jabber.org/protocol/disco#items">
              {results.map((table) => {
                let playerCount = table.occupants.filter(
                  (o) => o.role == "player"
                ).length;
                let watcherCount = table.occupants.filter(
                  (o) => o.role == "watcher"
                ).length;
                return xml("item", {
                  jid: table.table_id,
                  name: table.name,
                  playerCount,
                  watcherCount,
                });
              })}
            </query>
          );
        } else {
          // TODO: list stuff for that particular room
          return <query xmlns="http://jabber.org/protocol/disco#items" />;
        }
      }
    );

    // return search form
    this.xmpp.iqCallee.get("jabber:iq:search", "query", (ctx) => {
      console.log("returning search form");
      logger.info("returning search form");
      return (
        <query xmlns="jabber:iq:search">
          <instructions>
            Hi there, please enter what you want to search for
          </instructions>
          <x xmlns="jabber:x:data" type="form">
            <title>Table Search</title>
            <field type="hidden" var="FORM_TYPE">
              <value>jabber:iq:search</value>
            </field>
            <field
              type="text-single"
              label="Room Name"
              var="mug#roomsearch_name"
            />
            <field type="hidden" var="mug#roomsearch_game">
              <value>{this.standard}</value>
            </field>
          </x>
        </query>
      );
    });

    // process search form
    this.xmpp.iqCallee.set("jabber:iq:search", "query", (ctx) => {
      if (ctx.stanza.children) {
        // return results
        let x = ctx.stanza.getChild("query").getChild("x");
        if (x && x.attrs.xmlns == "jabber:x:data") {
          // define params
          var params = {};

          // insert values into params
          var fields = x.getChildren("field");
          for (let i = 0; i < fields.length; i++) {
            params[fields[i].attrs.var] = fields[i].getChildText("value");
          }

          // validate form type and filters
          if (params["FORM_TYPE"] != "jabber:iq:search") return;
          if (
            params["mug#roomsearch_game"] != "http://hool.org/protocol/mug#hool"
          ) {
            return <query xmlns="jabber:iq:search" />; // no non-hool results
          }

          this.engine.getTableList().then((results) => {
            return (
              <query xmlns="jabber:iq:search">
                <x xmlns="jabber:x:data" type="result">
                  <field type="hidden" var="FORM_TYPE">
                    <value>{params["FORM_TYPE"]}</value>
                  </field>
                  <reported>
                    <field var="time" label="Time" type="text-single" />
                    <field var="host" label="Host" type="jid-single" />
                    <field var="players" label="Players" type="text-single" />
                    <field var="watchers" label="Watchers" type="boolean" />
                    <field var="jid" label="JID" type="jid-single" />
                  </reported>
                  {results.map((table) => {
                    return (
                      <item>
                        <field var="jid">
                          <value>{table.table_id}</value>
                        </field>
                        <field var="players">
                          <value>{table.players}</value>
                        </field>
                        <field var="kibbitzers">
                          <value>{table.watchers}</value>
                        </field>
                      </item>
                    );
                  })}
                </x>
              </query>
            );
          });
        }
      }
    });

    // return registration form
    this.xmpp.iqCallee.get("jabber:iq:register", "query", async (iq) => {
      let userInfo = await this.engine.getUser(iq.from);
      if (userInfo) {
        return (
          <query xmlns="jabber:iq:register">
            <registered />
            <x xmlns="jabber:x:data" type="result">
              <field type="hidden" var="FORM_TYPE">
                <value>jabber:iq:register</value>
              </field>
              <field type="text-single" label="Username" var="username">
                <value>{userInfo.jid.local}</value>
              </field>
              <field type="text-private" label="Email" var="email" />
              <field type="text-single" label="Location" var="location">
                <value>{userInfo.location}</value>
              </field>
              <field type="text-single" label="Name" var="name">
                <value>{userInfo.name}</value>
              </field>
              <field type="text-single" label="Account Status" var="status">
                <value>{userInfo.status}</value>
              </field>
            </x>
          </query>
        );
      } else {
        return (
          <query xmlns="jabber:iq:register">
            <instructions>
              Use the enclosed form to register. If your Jabber client does not
              support Data Forms, visit the Hool website to register via your
              web browser.
            </instructions>
            <x xmlns="jabber:x:data" type="form">
              <title>Hool Registration</title>
              <instructions>
                Please enter your registration details. Your email will have to
                be verified before you can start using this service.
              </instructions>
              <field type="hidden" var="FORM_TYPE">
                <value>jabber:iq:register</value>
              </field>
              <field type="text-single" label="Username" var="username">
                <required />
              </field>
              <field type="text-single" label="Email" var="email">
                <required />
              </field>
              <field type="text-single" label="Name" var="name" />
              <field type="text-single" label="Location" var="location" />
            </x>
          </query>
        );
      }
    });

    // process registration form
    this.xmpp.iqCallee.set("jabber:iq:register", "query", async (iq) => {
      if (iq.stanza.attrs.to == this.domain) {
        if (
          iq.stanza.getChildren("query").length &&
          iq.stanza.getChild("query").attrs.xmlns == "jabber:iq:register" &&
          iq.stanza.getChild("query").getChildren("x").length
        ) {
          // handle account creation
          let xData = iq.stanza.getChild("query").getChild("x");

          let registration = {
            type: "register",
            from: iq.from,
          };

          for (let field of xData.getChildren("field")) {
            if (field.getChildren("value").length) {
              registration[field.attrs.var] = field.getChild("value").text();
            }
          }

          return await this.renderRegistration(
            await this.engine.handleRegistration(registration)
          );
        }
      }
    });

    // process room creation
    this.xmpp.iqCallee.set(
      "http://jabber.org/protocol/mug#owner",
      "query",
      (ctx) => {
        console.log(`ctx - ${ctx}`);
        // stanza will be directed to a certain room
        if (iq.stanza.attrs.to != this.domain) {
          let room_id = jid(iq.stanza.attrs.to);

          // check if user wants form returned
          if (
            iq.stanza.getChild("query") &&
            iq.stanza.getChild("query").getChild("options") &&
            iq.stanza.getChild("query").getChild("options").children.length == 0
          ) {
            return (
              <query xmlns="http://jabber.org/protocol/mug#owner">
                <options>
                  <x xmlns="jabber:x:data" type="form">
                    <title>Configuration for Tricko room</title>
                    <instructions>
                      Your room {room_id} has been created! The default
                      configuration is as follows: - Private: no To accept the
                      default configuration, click OK. To select a different
                      configuration option, please complete this form.
                    </instructions>
                    <field type="hidden" var="FORM_TYPE">
                      <value>http://jabber.org/protocol/mug#matchconfig</value>
                    </field>
                    <field
                      label="Include this room in the public lists?"
                      type="boolean"
                      var="mug#roomconfig_publicroom"
                    >
                      <value>1</value>
                    </field>
                  </x>
                  <options xmlns={this.standard}>
                    <x xmlns="jabber:x:data" type="form">
                      <title>Game settings</title>
                      <field var="FORM_TYPE">
                        <val>{this.standard + "#hoolconfig"}</val>
                      </field>
                      <field
                        label="Private (will not be listed)"
                        type="boolean"
                        var="mug/tricko#config_private"
                      >
                        <value>0</value>
                      </field>
                    </x>
                  </options>
                </options>
              </query>
            );
          }
        }
      }
    );
  }

  /**
   * Basic management functions
   */
  start() {
    if (process.env.NODE_ENV == "development") {
      debug(this.xmpp, true);
    }

    return this.xmpp.start();
  }

  stop() {
    return this.xmpp.stop();
  }

  /**
   * Listening for events!
   */
  listen(engine) {
    this.engine = engine;

    this.engine.on("roomJoined", (presence) => {
      if (!presence.to) throw "To whom are we sending this presence?";
      if (!presence.user) throw "Which `user` has joined?";
      if (!presence.room) throw "Which `room` has the user joined?";
      if (!presence.role) throw "What is the user's `role`?";
      if (!presence.nickname) throw "What is the user's `nickname`?";
      // Compute "from" message
      let from = jid(presence.room.toString());
      from.resource = presence.nickname;

      // Send it out!
      this.xmpp.send(
        <presence from={from} to={presence.to}>
          <game xmlns="http://jabber.org/protocol/mug">
            <item
              role={presence.role}
              affiliation="member"
              jid={presence.user}
            />
          </game>
        </presence>
      );
    });

    this.engine.on("roomExited", (presence) => {
      if (!presence.to) throw "To whom are we sending this presence?";
      if (!presence.user) throw "Which `user` has exited?";
      if (!presence.room) throw "Which `room` has the user exited?";
      if (!presence.nickname) throw "What was the user's `nickname`?";

      // Compute "from" message
      let from = jid(presence.room.toString());
      from.resource = presence.nickname;

      // Send it out!
      this.xmpp.send(
        <presence from={from} to={presence.to} type="unavailable">
          <game xmlns="http://jabber.org/protocol/mug">
            <item affiliation="none" role="none" jid={presence.user} />
          </game>
          {!!presence.destroy ? (
            <x xmlns="http://jabber.org/protocol/muc#user">
              <item affiliation="none" role="none" />
              <destroy jid={presence.room}>
                <reason>
                  {presence.destroy == true
                    ? "This room has been destroyed"
                    : presence.destroy}
                </reason>
              </destroy>
            </x>
          ) : null}
        </presence>
      );
    });

    this.engine.on("roomFull", (presence) => {
      if (!presence.user) throw "Which `user` is trying to join?";
      if (!presence.room) throw "Which `room` is the user trying to join?";

      this.xmpp.send(
        <presence from={presence.room} to={presence.user} type="error">
          <game xmlns="http://jabber.org/protocol/mug" />
          <error type="wait">
            <service-unavailable xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" />
          </error>
        </presence>
      );
    });

    this.engine.on("state", (state) => {
      if (!state.user) throw "Which `user` is this state update for?";
      if (!state.room) throw "For which `room` is this state update?";

      this.xmpp.send(
        <presence from={state.room} to={state.user}>
          {state.status ? <status>{state.status}</status> : null}
          <game xmlns="http://jabber.org/protocol/mug">
            <state xmlns={this.standard}>
              <x xmlns="jabber:x:data" type="submit">
                {state.deck ? (
                  <deck>
                    {state.deck.cards.map((card) => (
                      <card suit={card.suit} rank={card.rank} />
                    ))}
                  </deck>
                ) : null}
                {state.players ? (
                  <players>
                    {state.players.map((player, i) => (
                      <player
                        index={player.index || i}
                        inactive={player.inactive ? true : false}
                      >
                        {player.hand ? (
                          <hand
                            exposed={player.exposed === true ? "true" : "false"}
                          >
                            {player.hand.map((card) => (
                              <card suit={card.suit} rank={card.rank} />
                            ))}
                          </hand>
                        ) : null}

                        {typeof player.score == "number" &&
                        !isNaN(player.score) ? (
                          <score>{player.score}</score>
                        ) : null}
                      </player>
                    ))}
                  </players>
                ) : null}
                {state.tricks ? (
                  <tricks>
                    <trump>{state.tricks.trumpSuit}</trump>
                    {state.tricks.map((trick) => (
                      <trick flipped={trick.flipped ? "flipped" : null}>
                        {trick.map((play) => (
                          <card
                            suit={play.card.suit}
                            rank={play.card.rank}
                            player={play.player}
                          />
                        ))}
                      </trick>
                    ))}
                  </tricks>
                ) : null}
              </x>
            </state>
          </game>
        </presence>
      );
    });

    this.engine.on("start", (start) => {
      if (!start.to) throw "Whom is this start message being sent `to`?";
      if (!start.room) throw "In which `room` is this player ready to start?";
      if (!start.nickname) throw "What was the starter's `nickname`?";

      // Calculate "from" address
      let from = jid(start.room.toString());
      from.resource = start.nickname;

      this.xmpp.send(
        <message from={from} to={start.to} type="chat">
          <start xmlns="http://jabber.org/protocol/mug#user" />
        </message>
      );
    });

    this.engine.on("startError", (start) => {
      if (!start.user) throw "Which `user` tried to start?";
      if (!start.room) throw "In which `room` did the user try to start?";

      this.xmpp.send(
        <message from={start.room} to={start.user} type="chat">
          <start xmlns="http://jabber.org/protocol/mug#user" />
          <error type="cancel">
            <not-allowed xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" />
          </error>
        </message>
      );
    });
    this.engine.on("trump",(trump)=>{
      if (!trump.room) throw "In which `room` was made the change in trump?";
      if (!trump.nickname) throw "What is the  `nickname` of the player who changed the trumpValue?";
      if (typeof trump.player != "number") throw "Which `player` make the chnage in trump?";

      let from = jid(trump.room.toString());
      from.resource = trump.nickname;
      this.xmpp.send(
        <message from={from} to={trump.to} type="chat"> 
           <turn xmlns="http://jabber.org/protocol/mug#user">
            <trump player={trump.player} xmlns={this.standard}>{typeof trump.value == "string" ? trump.value : ""}</trump>
          </turn>
        </message>
      )
    })
    this.engine.on("trumpError", (trump) => {
      if (!trump.user) throw "Which {user} made the trump decision?";
      if (!trump.room)
        throw "In Which {room} did the user send the trump decision";
  
      // Calculate "from" address
      let from = jid(trump.room.toString());
      from.resource = trump.nickname;
  
      this.xmpp.send(
        <message from={trump.room} to={trump.user} type="chat">
          <error type="cancel">
            <not-allowed xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" />
          </error>
        </message>
      );
    });

    this.engine.on("card", (card) => {
      if (!card.to) throw "Whom is the card being sent `to`?";
      if (!card.room) throw "In which `room` was this card played?";
      if (!card.nickname) throw "What is the player's `nickname`?";
      if (typeof card.player != "number")
        throw "Which `player` played this card?";
      if (!card.suit) throw "What is the `suit` of the card?";
      if (!card.rank) throw "What is the `rank` of the card?";

      // Calculate "from" address
      let from = jid(card.room.toString());
      from.resource = card.nickname;

      this.xmpp.send(
        <message from={from} to={card.to} type="chat">
          <turn xmlns="http://jabber.org/protocol/mug#user">
            <card
              suit={card.suit}
              rank={card.rank}
              player={card.player}
              xmlns={this.standard}
            />
          </turn>
        </message>
      );
    });

    this.engine.on("cardError", (card) => {
      if (!card.user) throw "Which `user` tried to play a card?";
      if (!card.room) throw "In which `room` was this card played?";
      if (!card.suit) throw "What was the `suit` of the card?";
      if (!card.rank) throw "What was the `rank` of the card?";
      // card.player is optional

      // Decide error type
      let errorType = "auth";
      if (card.errorType && card.errorType == "invalid-turn") {
        errorType = "invalid-turn";
      }

      // Calculate "from" address
      let from = jid(card.room.toString());
      from.resource = card.nickname;

      this.xmpp.send(
        <message from={from} to={card.user} type="chat">
          <turn xmlns="http://jabber.org/protocol/mug#user">
            <card
              suit={card.suit}
              rank={card.rank}
              player={card.player}
              xmlns={this.standard}
            />
          </turn>
          <error type="cancel">
            {errorType == "auth" ? (
              <forbidden xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" />
            ) : (
              [
                <undefined-condition xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" />,
                <invalid-turn xmlns="http://jabber.org/protocol/mug" />,
              ]
            )}
            {card.errorText ? <text>{card.errorText}</text> : ""}
          </error>
        </message>
      );
    });

    this.engine.on("flip", (flip) => {
      if (!flip.to) throw "Whom is the flip being sent `to`?";
      if (!flip.room) throw "Which `room` was flipped?";
      if (!flip.nickname) throw "What is the player's `nickname`?";
      if (typeof flip.player != "number") throw "Which `player` flipped?";

      // Calculate "from" address
      let from = jid(flip.room.toString());
      from.resource = flip.nickname;

      this.xmpp.send(
        <message from={from} to={flip.to} type="chat">
          <turn xmlns="http://jabber.org/protocol/mug#user">
            <flip player={flip.player} xmlns={this.standard} />
          </turn>
        </message>
      );
    });

    this.engine.on("flipError", (flip) => {
      if (!flip.user) throw "Which `user` tried to flip?";
      if (!flip.room) throw "In which `room` was this flip attempted?";
      // flip.player is optional

      // Decide error type
      let errorType = "auth";
      if (flip.errorType && flip.errorType == "invalid-turn") {
        errorType = "invalid-turn";
      }

      // Calculate "from" address
      let from = jid(flip.room.toString());
      from.resource = flip.nickname;

      this.xmpp.send(
        <message from={from} to={flip.user} type="chat">
          <turn xmlns="http://jabber.org/protocol/mug#user">
            <flip player={flip.player} xmlns={this.standard} />
          </turn>
          <error type="cancel">
            {errorType == "auth" ? (
              <forbidden xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" />
            ) : (
              [
                <undefined-condition xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" />,
                <invalid-turn xmlns="http://jabber.org/protocol/mug" />,
              ]
            )}
            {flip.errorText ? <text>{flip.errorText}</text> : ""}
          </error>
        </message>
      );
    });

    this.engine.on("chat", (chat) => {
      if (!chat.to) throw "To which `user` this message?";
      if (!chat.room) throw "In which `room` did the user send the message?";
      if (!chat.nickname) throw "Specify the nickname of the user!";
      if (!chat.text) throw "What was the chat message?";

      // Calculate "from" address
      let from = jid(chat.room.toString());
      from.resource = chat.nickname;

      this.xmpp.send(
        <message from={from} to={chat.to} type="chat" nickname={chat.nickname}>
          <body>{chat.text}</body>
        </message>
      );
    });

    this.engine.on("chatError", (chat) => {
      if (!chat.user) throw "which {user} sent a message?";
      if (!chat.room) throw "In which {room} did the user send the message";

      // Calculate "from" address
      let from = jid(chat.room.toString());
      from.resource = chat.nickname;

      this.xmpp.send(
        <message from={chat.room} to={chat.user} type="chat">
          <error type="cancel">
            <not-allowed xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" />
          </error>
        </message>
      );
    });

    this.engine.on("take", (take) => {
      if (!take.to) throw "Whom is the take or Flip being sent `to`?";
      if (!take.room) throw "In Which `room` the player is making choice?";
      if (!take.nickname) throw "What is the player's `nickname`?";
      if (typeof take.player !== "number")
        throw "Which `player` is making choice?";

      // Calculate "from" address
      let from = jid(take.room.toString());
      from.resource = take.nickname;

      this.xmpp.send(
        <message from={from} to={take.to} type="chat" nickname={take.nickname}>
          <turn xmlns="http://jabber.org/protocol/mug#user">
            <take player={take.player} xmlns={this.standard} />
          </turn>
        </message>
      );
    });

    this.engine.on("takeError", (take) => {
      if (!take.user) throw "Which {user} made the take decision?";
      if (!take.room)
        throw "In Which {room} did the user send the take decision";

      // Calculate "from" address
      let from = jid(take.room.toString());
      from.resource = take.nickname;

      this.xmpp.send(
        <message from={take.room} to={take.user} type="chat">
          <error type="cancel">
            <not-allowed xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" />
          </error>
        </message>
      );
    });

    this.engine.on("flipAndTrump", (flipAndTrump) => {
      if (!flipAndTrump.to) throw "Whom is the flip being sent `to`?";
      if (!flipAndTrump.room) throw "Which `room` was flipped and trumpped?";
      if (!flipAndTrump.nickname) throw "What is the player's `nickname`?";
      if (typeof flipAndTrump.player != "number") throw "Which `player` flipped and trumpped?";

      // Calculate "from" address
      let from = jid(flipAndTrump.room.toString());
      from.resource = flipAndTrump.nickname;

    this.xmpp.send(
      <message from={from} to={flipAndTrump.to} type="chat">
        <turn xmlns="http://jabber.org/protocol/mug#user">
          <flipAndTrump player={flipAndTrump.player} xmlns={this.standard}>
             {typeof flipAndTrump.value == "string" ? flipAndTrump.value : ""}
           </flipAndTrump>
        </turn>
      </message>
    );
  });
  
  this.engine.on("flipAndTrumpError", (flipAndTrump) => {
    if (!flipAndTrump.user) throw "Which {user} made the flipAndTrump decision?";
    if (!flipAndTrump.room)
      throw "In Which {room} did the user send the flipAndTrump decision";

    // Calculate "from" address
    let from = jid(flipAndTrump.room.toString());
    from.resource = flipAndTrump.nickname;

    this.xmpp.send(
      <message from={flipAndTrump.room} to={flipAndTrump.user} type="chat">
        <error type="cancel">
          <not-allowed xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" />
        </error>
      </message>
    );
  });

  }
}
