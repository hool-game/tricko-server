import { jid } from "@xmpp/component";
import { EventEmitter } from "events";

import cards from "@firtoska/cards";
import Tricko from "@tricko/logic";

export class TrickoEngine extends EventEmitter {
  /**
   * Constructor for TrickoEngine
   *
   * Quickly set up and listen for Tricko events.
   * Currently supported options (inside the
   * `options` object) are:
   *
   * - redisConnection - the database connection
   * - transmitter - a TrickoTransmitter object
   *
   * More options may be added later.
   */

  constructor(options = {}, ...args) {
    // call the parent constructor first
    super(...args);

    if (!!options.db) {
      this.setDB(options.db);
    }

    if (!!options.transmitter) {
      this.listen(options.transmitter);
    }
  }

  /**
   * Connect to the Redis database
   */
  setDB(redisConnection) {
    this.db = redisConnection;
  }

  /**
   * Process events
   */

  processEvents(events) {
    events.forEach((e) => {
      this.emit(e.event, e);
    });
  }

  // room list functions (for jabber:iq:search)
  async getRooms() {
    return []; // TODO: implement
  }

  async getRoom(roomID, create = false) {
    // First, check if the room already exists
    let room = await this.db.json.get(`room:${roomID}`);
    /*
     * Create it if we have to
     *
     * Note that this only creates a placeholder
     * to be manipulated; it doesn't actually save
     * it to the database or anything like that
     */
    if (create && !room) {
      return {
        occupants: [],
        messages: [],
        game: {}, // empty game
      };
    }

    // Process the data a bit
    if (room) {
      // Unflatten the deck
      if (room.deck) {
        let originalDeck = room.deck;
        room.deck = new cards.Deck();
        room.deck.cards = originalDeck;
      }

      // Unflatten tricks
      if (room.tricks) {
        let originalTricks = room.tricks;
        room.tricks = new cards.Tricks();

        room.tricks.loadArray(originalTricks);
        room.tricks.trumpSuit = room.meta.trumpSuit;
        if (room.meta?.trumpSuit) {
          room.meta.trumppedTricks.forEach((trickTrump, i) => {
            room.tricks[i].trumpSuit = trickTrump;
          });
        }
        // Flip appropriate tricks
        if (room.meta?.flippedTricks) {
          room.meta.flippedTricks.forEach((flipped, i) => {
            if (flipped && room.tricks[i]) {
              room.tricks[i].flipped = true;
            }
          });

          // Don't forget to flip the tricks object itself!
          if (
            room.tricks.length &&
            room.tricks[room.tricks.length - 1].flipped
          ) {
            room.tricks.flipped = true;
          }
        }
      }
    }

    // Return it
    return room;
  }

  async setRoom(roomID, data) {
    // Don't munge the data
    let d = { ...data };
  

    // Extra data that should not be used outside the
    // getRoom and setRoom functions
    d.meta = {};

    // Flatten the deck
    if (d.deck && d.deck.cards) {
      d.deck = d.deck.cards;
    }

    // Flatten tricks
    if (d.tricks && d.tricks.getTricksArray) {
      // First, note down which tricks are flipped
      d.meta.flippedTricks = d.tricks.map((trick) => trick.flipped);

      // We setting the trumpSuit in handleStart(),
      // but we need to set it here also to save the trump
      if (d.tricks.length > 1) {
        d.meta.trumpSuit = d.tricks[d.tricks.length - 1].trumpSuit;
      } else {
        d.meta.trumpSuit = d.tricks.trumpSuit;
      }
      d.meta.trumppedTricks = d.tricks.map((trick) => trick.trumpSuit);

      // Then, flatten them
      d.tricks = d.tricks.getTricksArray();
    }

    // Save it
    return await this.db.json.set(`room:${roomID}`, "$", d);
  }

  async delRoom(roomID) {
    await this.db.del(`room:${roomID}`);
  }

  // Helper function to send state updates
  renderState(roomID, room, userIndex, render = []) {
    // Check the user index
    if (!room.occupants[userIndex]) {
      throw `Invalid userIndex: ${userIndex}`;
    }

    // Decide what to render
    let toRender = new Map(
      ["occupants", "ready", "deck", "hands", "bids", "score", "tricks"].map(
        (p) => [p, false]
      )
    );

    // Parse the "render" argument
    if (render == "all") {
      // 'all' is shorthand for rendering everything
      toRender.forEach((_, r) => toRender.set(r, true));
    } else if (typeof render == "string") {
      // If it's a string, we just render that one thing
      toRender.set(render, true);
    } else {
      // Otherwise, mark each thing separately
      render.forEach((r) => {
        // 'game' is shorthand for hands, bids, and tricks
        if (r == "game") {
          ["hands", "bids", "score", "tricks", "deck"].forEach((s) =>
            toRender.set(s, true)
          );
        } else {
          toRender.set(r, true);
        }
      });
    }

    // Now, we have a handy map of what to render and
    // what to ignore.

    let events = [];

    // First, get some basic info
    let user = room.occupants[userIndex].jid;
    let nickname = room.occupants[userIndex].nickname;

    let side = -1;
    if (typeof room.occupants[userIndex].player == "number") {
      side = room.occupants[userIndex].player;
    }

    // Start with sending the occupant list
    if (toRender.get("occupants")) {
      room.occupants.forEach((o) => {
        events.push({
          event: "roomJoined",
          to: user,
          user: o.jid,
          room: roomID,
          role: o.role || "player",
          nickname: o.nickname,
        });
      });
    }

    // Next, tell if they're ready to play
    if (toRender.get("ready")) {
      room.occupants.forEach((o) => {
        if (!!o.ready) {
          events.push({
            event: "start",
            to: user,
            nickname: o.nickname,
            room: roomID,
          });
        }
      });
    }

    // Now it's time to start rendering the state!
    if (
      ["deck", "hands", "bids", "score", "tricks"].some((r) => toRender.get(r))
    ) {
      // Set up the event itself
      let state = {
        event: "state",
        user: user,
        room: roomID,
      };

      // Begin with the deck
      if (
        toRender.get("deck") &&
        !!room.deck?.cards // The deck should exist!
      ) {
        state.deck = room.deck;
      }

      // Move on to the per-player data: hands and bids
      if (
        ["hands", "bids", "score"].some((r) => toRender.get(r)) &&
        !!room.players // The players should exist!
      ) {
        state.players = [];

        room.players.forEach((p, i) => {
          let player = {};
          player.nickname = p.nickname;
          player.inactive = p.inactive;
          player.index = i;

          // Include hands
          if (toRender.get("hands")) {
            // Only show hands if it's an own hand, the
            // user is a non-player, or the hand is open
            if (
              side == i ||
              side < 0 || // less than zero means non-player
              !!p.exposed
            ) {
              player.hand = p.hand;

              if (!!p.exposed) player.exposed = p.exposed;
            }
          }

          // Include bids
          if (toRender.get("bids")) {
            // Only reveal bids if it's an own bid or the
            // user is a non-player
            if (
              side == i ||
              side < 0 // less than zero means non-player
            ) {
              player.bid = p.bid;
            } else {
              // Otherwise, we just say whether they've bid
              // or not.
              player.bid = typeof p.bid == "number";
            }
          }

          // Include scores
          if (toRender.get("score")) {
            // No conditions for this one ;)
            player.score = p.score;

            //reveal the bid also
            player.bid = p.bid;
          }

          // Don't forget to add it all in
          state.players.push(player);
        });
      }

      // Dealers should be created in handleGameStart() - if

      // Now the players are rendered (in case you lost track).
      // All that's left is to do the tricks

      if (
        toRender.get("tricks") &&
        !!room.tricks // the tricks should exist!
      ) {
        // Nothing much to do here; it's already formatted
        // properly for us (at least I hope so!)
        state.tricks = room.tricks;
      }

      // That's it for the state update
      events.push(state);
    }

    // ...and we're done!
    return events;
  }

  /**
   * Let the games begin!
   */
  async handleRoomJoin(request) {
    console.debug(
      `${request.user} wants to join ${request.room} as ${request.role}`
    );
    let events = [];
    let room = await this.getRoom(request.room, true);

    // If the user is already there, add them anyway
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(request.user).bare().toString()
    );

    if (userIndex >= 0) {
      room.occupants[userIndex].jid = jid(request.user).bare().toString();

      // Send the game state, just in case (this will
      // include a "you joined" event as well as all
      // the "someone else joined" events for everyone
      // else.
      events = events.concat(
        this.renderState(request.room, room, userIndex, "all")
      );

      return events;
    }

    // Check if we're full
    if (room.occupants.length >= 6) {
      console.debug("Skipping: room is full");

      // TODO: allow them to join as non-players

      // Send "room full" message
      events.push({
        event: "roomFull",
        room: request.room,
        user: request.user,
      });

      // TODO: remove this line
      this.delRoom(request.room); // clearing for testing

      return events;
    }

    // Check if the game has already started

    if (room.started) {
      console.debug("Banning: game has already started");

      // Send "room full" message
      // TODO: change the message
      events.push({
        event: "roomFull",
        room: request.room,
        user: request.user,
      });

      return events;
    }

    // TODO: handle mid-game joins

    // All good; add us in
    room.occupants.push({
      jid: jid(request.user).bare().toString(),
      role: request.role || "player", // TODO: change default
      nickname: request.nickname,
    });

    // Note down the userIndex
    userIndex = room.occupants.length - 1;
    // ^-- that was mildly unsafe but should be okay

    // Save the room
    this.setRoom(request.room, room);

    // Tell everyone about it
    room.occupants.forEach((o) => {
      // user -> everyone (except self)
      if (o.jid == jid(request.user).bare()) return;

      events.push({
        event: "roomJoined",
        to: o.jid,
        user: request.user,
        room: request.room,
        role: request.role || "player",
        nickname: request.nickname,
      });
    });

    // Send the user a full update
    events = events.concat(
      this.renderState(request.room, room, userIndex, "all")
    );

    return events;
  }

  async handleRoomExit(request) {
    console.debug(`${request.user} has left ${request.room}.`);

    let events = [];

    // fetch the room
    let room = await this.getRoom(request.room);
    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${request.room} does not exist; ignoring.`);
      return events;
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(request.user).bare().toString()
    );

    // if the user is not in the room, there's nothing to do!
    if (userIndex < 0) {
      console.debug(`${request.user} is not in ${request.room}; ignoring.`);
      return events;
    }

    // remove the user
    let user = room.occupants.splice(userIndex, 1)[0];

    // find out how many users are left
    if (room.occupants.length == 0) {
      // no users? destroy the table!

      this.delRoom(request.room);
      return events;
    } else {
      // tell everyone
      room.occupants.forEach((o) =>
        events.push({
          event: "roomExited",
          to: o.jid,
          user: user.jid,
          room: request.room,
          nickname: user.nickname,
        })
      );

      // if the disconnected user is a player means
      // we have to remove the user from player list and
      // reveal his cards

      if (user.role === "player") {
        let playerIndex = user.player;

        if (room?.players) {
          // find the active players
          let activeplayers = room.players.filter((player) => !player.inactive);

          // change the flag to true

          if (
            typeof playerIndex !== "undefined" &&
            room?.dealers[playerIndex]
          ) {
            room.dealers[playerIndex].inactive = true;
          }

          if (
            typeof playerIndex !== "undefined" &&
            room?.players[playerIndex]
          ) {
            room.players[playerIndex].inactive = true;

            // reveal the player hand , if the active players length is greater than three
            if (activeplayers.length > 3) {
              room.players[playerIndex].exposed = true;

              room.occupants.forEach((_, index) => {
                events = events.concat(
                  this.renderState(request.room, room, index, ["hands"])
                );
              });
            }
          }
        }
      }

      // save the room
      this.setRoom(request.room, room);

      return events;
    }
  }

  async handleRoomDisconnect(request) {
    console.debug(`${request.user} has disconnected from ${request.room}.`);

    // TODO: process differently if needed
    return this.handleRoomExit(request);
  }

  async handleGameStart(request) {
    console.debug(`${request.user} is ready to play at ${request.room}`);

    let events = [];

    // fetch the room
    let room = await this.getRoom(request.room);

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${request.room} does not exist; ignoring.`);
      return events;
    }

    let tricko = new Tricko(room);

    // find the user
    let userIndex = tricko.data.occupants.findIndex(
      (o) => o.jid && o.jid == jid(request.user).bare().toString()
    );

    // if the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${request.user} is not in ${request.room}; rejecting.`);

      events.push({
        event: "startError",
        user: request.user,
        room: request.room,
      });
      return events;
    }

    // If the game has already started, reject it
    // Added the replay code for continuos play, if the player's turn is not score only
    // it should emit the Reject event.

    if (tricko.data.started && !tricko.getTurn() === "score") {
      console.debug(`A game is already ongoing at ${request.room}; rejecting.`);

      events.push({
        event: "startError",
        user: request.user,
        room: request.room,
      });
      return events;
    }

    // If the user has already said they're ready,
    // pretend to take action but don't actually do
    // anything
    if (!!tricko.data.occupants[userIndex].ready) {
      console.debug(
        `${request.user} has already said they're ready; ignoring.`
      );

      events.push({
        event: "start",
        to: request.user,
        nickname: tricko.data.occupants[userIndex].nickname,
        room: request.room,
      });
      return events;
    }

    // If the user hasn't said it before, then it's
    // a sign we should actually get up and work
    tricko.data.occupants[userIndex].ready = true;

    // Tell everyone about it
    tricko.data.occupants.forEach((o) => {
      events.push({
        event: "start",
        to: o.jid,
        nickname: tricko.data.occupants[userIndex].nickname,
        room: request.room,
      });
    });

    // But wait, there's more! If everyone is ready
    // we need to start the game!
    let actualPlayers = tricko.data.occupants.filter(
      (occupant) => occupant.role === "player"
    );
    let activeDealers = [];
    const dealerChange = () => {
      // Checking if the user is active or not
      activeDealers = room.dealers
        .map((p) => ({ ...p }))
        .filter((p) => !p.inactive);

      const dealerIndex = activeDealers.findIndex((dealer) => dealer.dealer);
      if (dealerIndex !== -1) {
        activeDealers.forEach((dealer, index) => {
          if (index === dealerIndex) {
            dealer.dealer = false;
          } else if (index === (dealerIndex + 1) % activeDealers.length) {
            dealer.dealer = true;
          } else {
            dealer.dealer = false;
          }
        });
      } else if (dealerIndex == -1) {
        activeDealers.forEach((dealer, index) => {
          if (index == tricko.data.dealer) {
            dealer.dealer = true;
          } else {
            dealer.dealer = false;
          }
        });
      }

      return activeDealers;
      // TODO: Check is the user still on the game. i.e., is he an occupant
    };

    if (
      actualPlayers.length > 1 &&
      tricko.data.occupants.every((o) => !!o.ready)
    ) {
      // Creating the new dealer array,. in tricko data - if
      // I'm creating before the players
      if (!room.dealers) {
        tricko.data.dealers = [];

        actualPlayers.forEach((o, i) => {
          let dealer = {
            nickname: o.nickname,
            inactive: false,
            dealer: i === 0 ? true : false,
          };

          tricko.data.dealers[i] = dealer;
        });
      } else {
        tricko.data.dealers = dealerChange();
      }
    }

    // --------------Dealers--------------- //

    if (
      actualPlayers.length > 1 &&
      tricko.data.occupants.every((o) => !!o.ready)
    ) {
      tricko.data.started = true;

      // Count the players
      tricko.data.players = [];

      actualPlayers.forEach((o, i) => {
        let player = {
          nickname: o.nickname,
          bid: null,
        };

        o.player = i;
        tricko.data.players[i] = player;
      });

      // Set up the deck
      tricko.data.deck = new cards.Deck();

      // Deal out the cards
      tricko.dealCards().hands.forEach((hand, i) => {
        tricko.data.players[i].hand = hand;
      });

      // Prepare the tricks space
      tricko.data.tricks = new cards.Tricks();

      //setting trump randomly from the below trumpingSuits items
      const trumpingSuits = ["S", "H", "C", "D", "NT", "S", "NT"];
      let randomTrump = trumpingSuits[Math.floor(Math.random() * 7)];
      tricko.data.tricks.trumpSuit =
        randomTrump === "NT" ? undefined : randomTrump;
      
      activeDealers = room.dealers
        ?.map((p) => ({ ...p }))
        ?.filter((p) => !p.inactive);
      tricko.data.dealer = activeDealers.findIndex((dealer) => dealer.dealer);
      // Broadcast it all
      tricko.data.occupants.forEach((_, occupantIndex) => {
        events = events.concat(
          this.renderState(request.room, tricko.data, occupantIndex, ["game"])
        );
      });
    }
    // Save the changes and return
    this.setRoom(request.room, tricko.data);
    return events;
  }

  async handleCard(card) {
    console.debug(
      `${card.user} has played ${card.suit} of ${card.rank} in ${card.room}`
    );

    let events = [];

    // fetch the room
    let room = await this.getRoom(card.room);

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${card.room} does not exist; ignoring.`);
      return events;
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(card.user).bare().toString()
    );

    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${card.user} is not in ${card.room}; rejecting.`);

      events.push({
        event: "cardError",
        user: card.user,
        room: card.room,
        player: card.player,
        suit: card.suit,
        rank: card.rank,
        errorText: "You do not belong to this room",
      });
      return events;
    }

    // If the user is not a player, reject them
    let player = room.occupants[userIndex].player;
    if (typeof player != "number" || player < 0) {
      console.debug(`${card.user} is not a player in ${card.room}; rejecting`);

      events.push({
        event: "cardError",
        user: card.user,
        room: card.room,
        player: card.player,
        suit: card.suit,
        rank: card.rank,
        errorText: "You are not a player in this room",
      });
      return events;
    }

    // If the game has not yet started, reject it
    if (!room.started) {
      console.debug(`The game at ${card.room} has not yet started!`);

      events.push({
        event: "cardError",
        user: card.user,
        room: card.room,
        player: card.player,
        suit: card.suit,
        rank: card.rank,
        errorType: "invalid-turn",
        errorText: "The game has not yet started",
      });
      return events;
    }

    // Load the Tricko logic!
    let tricko = new Tricko(room);

    // Find out whose turn it is
    let turn = tricko.getTurn();
    // Only play if it's our turn *and* it's time to
    // play cards
  
    if (turn.type != "card" || !turn.players.includes(player)) {
      console.debug(
        `Invalid turn from ${card.user} at ${card.room}; rejecting`
      );

      events.push({
        event: "cardError",
        user: card.user,
        room: card.room,
        player: card.player,
        suit: card.suit,
        rank: card.rank,
        errorType: "invalid-turn",
        errorText: "It is not your turn to play a card",
      });
      return events;
    }

    // Figure out what the valid cards are...
    let validCards = tricko.data.tricks.getValidCards(
      tricko.data.players[player].hand
    );

    // and make sure ours in included
    if (!validCards.find((c) => c.rank == card.rank && c.suit == card.suit)) {
      console.debug(
        `Card ${card.rank} of ${card.suit} cannot be played by ${card.user}; rejecting`
      );

      events.push({
        event: "cardError",
        user: card.user,
        room: card.room,
        player: card.player,
        suit: card.suit,
        rank: card.rank,
        errorType: "invalid-turn",
        errorText: "You cannot play that card now",
      });
      return events;
    }

    // Actually play the card!
    tricko.play(player, {
      suit: card.suit,
      rank: card.rank,
    });

    // Add the next trick if necessary
    if (tricko.isTrickComplete()) {
      tricko.data.tricks.addTrick();
    }

    // Tell everyone about it
    let nick = tricko.data.players[player].nickname;
    tricko.data.occupants.forEach((o) => {
      events.push({
        event: "card",
        to: o.jid,
        player: player,
        room: card.room,
        player: player,
        nickname: nick,
        suit: card.suit,
        rank: card.rank,
      });
    });

    turn = tricko.getTurn();
    if (turn.type == "score") {
      tricko.data.occupants.forEach((o) => {
        o.ready = false;
      });

      tricko.data.started = false;

      // for new user
      this.sendScore(tricko.data, card.room);

      events = events.concat(this.sendScore(tricko.data, card.room));
      delete tricko.data.players;
      delete tricko.data.tricks;
    }

    // Save the changes and return
    this.setRoom(card.room, tricko.data);
    return events;
  }

  async handleTake(take) {
    console.debug(
      `${take.user} is attempting to take the trick in ${take.room}`
    );

    let events = [];

    // fetch the room
    let room = await this.getRoom(take.room);

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${take.room} does not exist; ignoring.`);
      return events;
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(take.user).bare().toString()
    );
    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${take.user} is not in ${take.room}; rejecting.`);

      events.push({
        event: "takeError",
        user: take.user,
        room: take.room,
        player: take.player,
        errorText: "You do not belong to this room",
      });
      return events;
    }

    // If the user is not a player, reject them
    let player = room.occupants[userIndex].player;
    if (typeof player != "number" || player < 0) {
      console.debug(`${take.user} is not a player in ${take.room}; rejecting`);

      events.push({
        event: "takeError",
        user: take.user,
        room: take.room,
        player: take.player,
        errorText: "You are not a player in this room",
      });
      return events;
    }

    // If the game has not yet started, reject it
    if (!room.started) {
      console.debug(`The game at ${take.room} has not yet started!`);

      events.push({
        event: "takeError",
        user: take.user,
        room: take.room,
        player: take.player,
        errorType: "invalid-turn",
        errorText: "The game has not yet started",
      });
      return events;
    }

    // Load the Tricko logic!
    let tricko = new Tricko(room);

    // Find out whose turn it is
    let turn = tricko.getTurn();
    // Only take if it's our turn *and* it's time to
    // take (or flip) the trick
    if (
      (turn.type != "take" && turn.type != "card" && turn.type != "score") ||
      !turn.players.includes(player)
    ) {
      console.debug(
        `Invalid turn from ${take.user} at ${take.room}; rejecting cuz of bug `
      );

      events.push({
        event: "takeError",
        user: take.user,
        room: take.room,
        player: take.player,
        errorType: "invalid-turn",
        errorText: "It is not your turn to take the trick (or flip)",
      });
      return events;
    }
    // Actually take the trick!
    let trickIndex = tricko.data.tricks.length - 1;
    tricko.take(player, trickIndex);

    // Tell everyone about it
    let nick = tricko.data.players[userIndex].nickname;
    tricko.data.occupants.forEach((o) => {
      events.push({
        event: "take",
        to: o.jid,
        player: player,
        room: take.room,
        player: player,
        nickname: nick,
      });
    });

    // if the last card has been played and then
    // the player chooses to take the trick means
    // we have to send the score
    turn = tricko.getTurn();
    if (turn.type == "score") {
      tricko.data.occupants.forEach((o) => {
        o.ready = false;
      });

      tricko.data.started = false;

      // for new user
      this.sendScore(tricko.data, take.room);

      events = events.concat(this.sendScore(tricko.data, take.room));
      delete tricko.data.players;
      delete tricko.data.tricks;
    }

    // Save the changes and return

    this.setRoom(take.room, tricko.data);
    return events;
  }

  async handleFlip(flip) {
    console.debug(`${flip.user} has flipped in ${flip.room}`);

    let events = [];

    // fetch the room
    let room = await this.getRoom(flip.room);

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${flip.room} does not exist; ignoring.`);
      return events;
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(flip.user).bare().toString()
    );

    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${flip.user} is not in ${flip.room}; rejecting.`);

      events.push({
        event: "flipError",
        user: flip.user,
        room: flip.room,
        player: flip.player,
        errorText: "You do not belong to this room",
      });
      return events;
    }

    // If the user is not a player, reject them
    let player = room.occupants[userIndex].player;
    if (typeof player != "number" || player < 0) {
      console.debug(`${flip.user} is not a player in ${flip.room}; rejecting`);

      events.push({
        event: "flipError",
        user: flip.user,
        room: flip.room,
        player: flip.player,
        errorText: "You are not a player in this room",
      });
      return events;
    }

    // If the game has not yet started, reject it
    if (!room.started) {
      console.debug(`The game at ${flip.room} has not yet started!`);

      events.push({
        event: "flipError",
        user: flip.user,
        room: flip.room,
        player: flip.player,
        errorType: "invalid-turn",
        errorText: "The game has not yet started",
      });
      return events;
    }

    // Load the Tricko logic!
    let tricko = new Tricko(room);

    // Find out whose turn it is
    let turn = tricko.getTurn();

    // Only play if it's our turn *and* it's time to
    // flip (or take) the trick
    if (!turn.players.includes(player)) {
      console.debug(
        `Invalid turn from ${flip.user} at ${flip.room}; rejecting`
      );

      events.push({
        event: "flipError",
        user: flip.user,
        room: flip.room,
        player: flip.player,
        errorType: "invalid-turn",
        errorText: "It is not your turn to play a card (or flip)",
      });
      return events;
    }
    
    if (tricko.isTrickComplete()) {
      tricko.data.tricks.addTrick();
    }
    // Actually flip!
    let trickIndex = tricko.data.tricks.length - 1;
    if (tricko.isTrickComplete(trickIndex - 1)) {
      tricko.data.tricks.flip(tricko.data.tricks.length);
    }
    // to flip at end of each tricks
    // Tell everyone about it
    let nick = tricko.data.players[userIndex].nickname;
    tricko.data.occupants.forEach((o) => {
      events.push({
        event: "flip",
        to: o.jid,
        player: player,
        room: flip.room,
        player: player,
        nickname: nick,
      });
    });
    // if the last card has been played and then
    // the player chooses to take the trick means
    // we have to send the score
    turn = tricko.getTurn();
    if (turn.type == "score") {
      tricko.data.occupants.forEach((o) => {
        o.ready = false;
      });
      tricko.data.started = false;
      // for new user

      this.sendScore(tricko.data, flip.room);

      events = events.concat(this.sendScore(tricko.data, flip.room));
      delete tricko.data.players;
      delete tricko.data.tricks;
    }

    // Save the changes and return

    this.setRoom(flip.room, tricko.data);
    return events;
  }

  async handleTrump(trump) {
    console.debug(`${trump.user} has flipped in ${trump.room}`);

    let events = [];

    // fetch the room
    let room = await this.getRoom(trump.room);

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${trump.room} does not exist; ignoring.`);
      return events;
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(trump.user).bare().toString()
    );

    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${trump.user} is not in ${trump.room}; rejecting.`);

      events.push({
        event: "trumpError",
        user: trump.user,
        room: trump.room,
        player: trump.player,
        value: trump.value,
        errorText: "You do not belong to this room",
      });
      return events;
    }

    // If the user is not a player, reject them
    let player = room.occupants[userIndex].player;
    if (typeof player != "number" || player < 0) {
      console.debug(
        `${trump.user} is not a player in ${trump.room}; rejecting`
      );

      events.push({
        event: "trumpError",
        user: trump.user,
        room: trump.room,
        player: trump.player,
        value: trump.value,
        errorText: "You are not a player in this room",
      });
      return events;
    }

    // If the game has not yet started, reject it
    if (!room.started) {
      console.debug(`The game at ${trump.room} has not yet started!`);

      events.push({
        event: "trump",
        user: trump.user,
        room: trump.room,
        player: trump.player,
        value: trump.value,
        errorType: "invalid-turn",
        errorText: "The game has not yet started",
      });
      return events;
    }

    // Load the Tricko logic!
    let tricko = new Tricko(room);

    // Find out whose turn it is
    let turn = tricko.getTurn();

    // We can include a "|| !turn.players.includes(player)"
    // here if we don't want to let people keep changing their
    // bids.
    if (!turn.players.includes(player)) {
      console.debug(
        `Invalid turn from ${trump.user} at ${trump.room}; rejecting`
      );
      events.push({
        event: "trumpError",
        user: trump.user,
        room: trump.room,
        player: trump.player,
        value: trump.value,
        errorType: "invalid-turn",
        errorText: "It is not your turn to change trump",
      });
      return events;
    }

    if (tricko.isTrickComplete()) {
      tricko.data.tricks.addTrick();
    }
    //if trump.value is "NT" then we assign hte value as undefined else the same valu remains itself;
    trump.value = trump.value == "NT" ? undefined : trump.value;
    //Assigning the trmupValue to current trick;
    tricko.data.tricks[tricko.data.tricks.length - 1].trumpSuit = trump.value;
    //Assigning the trmupValue to whole tricks;

    // Tell everyone about it
    let nick = tricko.data.players[player].nickname;
    tricko.data.occupants.forEach((o) => {
      events.push({
        event: "trump",
        to: o.jid,
        player: player,
        room: trump.room,
        player: player,
        nickname: nick,
        value: trump.value,
      });
    });

    // Save the changes and return
    this.setRoom(trump.room, tricko.data);
    return events;
  }
  async handleChat(chat) {
    console.debug(
      `${chat.from} sent a message of ${chat.text} to ${chat.room}`
    );

    let events = [];

    // fetch the room
    let room = await this.getRoom(chat.room);
    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${chat.room} does not exist; ignoring.`);
      return events;
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(chat.from).bare().toString()
    );

    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(`${chat.user} is not in ${chat.room}; rejecting.`);

      events.push({
        event: "chatError",
        user: chat.from,
        room: chat.room,
        errorText: "You do not belong to this room",
      });
      return events;
    }

    let nickname = room.occupants[userIndex].nickname;

    let message = {
      nickname,
      from: chat.from,
      text: chat.text,
    };

    //store the message
    room.messages.push(message);

    //send message to every one
    room.occupants.forEach((o) => {
      events.push({
        event: "chat",
        to: o.jid,
        room: chat.room,
        text: chat.text,
        nickname: nickname,
      });
    });

    // Save the changes and return
    this.setRoom(chat.room, room);
    return events;
  }
  
  async handleFlipAndTrump(flipAndTrump) {
    let events = [];

    // fetch the room
    let room = await this.getRoom(flipAndTrump.room);

    // if the room doesn't exist, there's nothing to do!
    if (!room) {
      console.debug(`${flipAndTrump.room} does not exist; ignoring.`);
      return events;
    }

    // find the user
    let userIndex = room.occupants.findIndex(
      (o) => o.jid && o.jid == jid(flipAndTrump.user).bare().toString()
    );

    // If the user is not in the room, reject them
    if (userIndex < 0) {
      console.debug(
        `${flipAndTrump.user} is not in ${flipAndTrump.room}; rejecting.`
      );

      events.push({
        event: "flipAndTrumpError",
        user: flipAndTrump.user,
        room: flipAndTrump.room,
        player: flipAndTrump.player,
        value: flipAndTrump.value,
        errorText: "You do not belong to this room",
      });
      return events;
    }

    // If the user is not a player, reject them
    let player = room.occupants[userIndex].player;
    if (typeof player != "number" || player < 0) {
      console.debug(
        `${flipAndTrump.user} is not a player in ${flipAndTrump.room}; rejecting`
      );

      events.push({
        event: "flipAndTrumpError",
        user: flipAndTrump.user,
        room: flipAndTrump.room,
        player: flipAndTrump.player,
        value: flipAndTrump.value,
        errorText: "You are not a player in this room",
      });
      return events;
    }

    // If the game has not yet started, reject it
    if (!room.started) {
      console.debug(`The game at ${flipAndTrump.room} has not yet started!`);

      events.push({
        event: "flipAndTrump",
        user: flipAndTrump.user,
        room: flipAndTrump.room,
        player: flipAndTrump.player,
        value: flipAndTrump.value,
        errorType: "invalid-turn",
        errorText: "The game has not yet started",
      });
      return events;
    }

    // Load the Tricko logic!
    let tricko = new Tricko(room);

    // Find out whose turn it is
    let turn = tricko.getTurn();

    // We can include a "|| !turn.players.includes(player)"
    // here if we don't want to let people keep changing their

    if (
      !turn.players.includes(player))
    {
      console.debug(
        `Invalid turn from ${flipAndTrump.user} at ${flipAndTrump.room}; rejecting`
      );
      events.push({
        event: "flipAndTrumpError",
        user: flipAndTrump.user,
        room: flipAndTrump.room,
        player: flipAndTrump.player,
        value: flipAndTrump.value,
        errorType: "invalid-turn",
        errorText: "It is not your turn to change trump & Flip",
      });
      return events;
    }

    if (tricko.isTrickComplete()) {
      tricko.data.tricks.addTrick();
    }

    //if trump.value is "NT" then we assign hte value as undefined else the same valu remains itself;

    flipAndTrump.value =
      flipAndTrump.value == "NT" ? undefined : flipAndTrump.value;

    //Assigning the trmupValue to current trick;

    tricko.data.tricks[tricko.data.tricks.length - 1].trumpSuit =
      flipAndTrump.value;
    //Assigning the trmupValue to whole tricks;

    let trickIndex = tricko.data.tricks.length - 1;
    if (tricko.isTrickComplete(trickIndex - 1)) {
      tricko.data.tricks.flip(tricko.data.tricks.length);
    }

    // Tell everyone about it
    let nick = tricko.data.players[player].nickname;
    tricko.data.occupants.forEach((o) => {
      events.push({
        event: "flipAndTrump",
        to: o.jid,
        player: player,
        room: flipAndTrump.room,
        player: player,
        nickname: nick,
        value: flipAndTrump.value,
      });
    });

    // Save the changes and return
    this.setRoom(flipAndTrump.room, tricko.data);
    return events;
  }

  sendScore(room, roomID) {
    let tricko = new Tricko(room);
    let score = tricko.getDealScore();
   
    score.forEach((s, i) => {
      tricko.data.players[i].score = s;
    });

    let events = [];
    tricko.data.occupants.forEach((_, occupantIndex) => {
      events.push(
        ...this.renderState(roomID, tricko.data, occupantIndex, ["score"])
      );
    });

    return events;
  }

  /**
   * Listeners for incoming events
   */
  addHandler(event, handler) {
    if (!this.transmitter) {
      throw "No transmitter specified! Please run the listen() command first";
    }

    // Make sure we have "this" accessible
    handler = handler.bind(this);

    this.transmitter.on(event, async (e) =>
      this.processEvents(await handler(e))
    );
  }

  listen(transmitter) {
    this.transmitter = transmitter;

    this.addHandler("roomJoin", this.handleRoomJoin);
    this.addHandler("roomDisconnect", this.handleRoomDisconnect);
    this.addHandler("roomExit", this.handleRoomExit);
    this.addHandler("start", this.handleGameStart);
    this.addHandler("card", this.handleCard);
    this.addHandler("flip", this.handleFlip);
    this.addHandler("chat", this.handleChat);
    this.addHandler("take", this.handleTake);
    this.addHandler("trump", this.handleTrump);
    this.addHandler("flipAndTrump", this.handleFlipAndTrump);
  }
}
